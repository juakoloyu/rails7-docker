FROM ruby:3.0.2-alpine 

RUN apk add --update --virtual \
  runtime-deps \
  postgresql-client \
  build-base \
  libxml2-dev \
  libxslt-dev \
  mysql-client \
  mariadb-dev \
  sqlite-dev \
  nodejs \
  yarn \
  libffi-dev \
  readline \
  build-base \
  postgresql-dev \
  libc-dev \
  linux-headers \
  readline-dev \
  file \
  git \
  tzdata \
  && rm -rf /var/cache/apk/*

WORKDIR /app
COPY Gemfile Gemfile.lock /app/

ENV BUNDLE_PATH /gems
RUN yarn install


RUN gem install bundler -v 2.2.29 
RUN bundle install
#RUN bundle exec rails action_text:install
COPY . /app/

#FROM ruby:2.7.1-alpine
#COPY --from=builder /app /app

#ENTRYPOINT ["bin/rails"]
#CMD ["s", "-b", "0.0.0.0"]
CMD ["sh"]

EXPOSE 3000