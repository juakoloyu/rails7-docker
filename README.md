# README

## Si aun no está creado el proyecto:
````
docker-compose run --no-deps -v "$(pwd):/app" ruby_dev bundle exec rails new . --force --database=mysql 
````

## Si se agrega una gema es necesario buildear:
````
docker-compose build
````
